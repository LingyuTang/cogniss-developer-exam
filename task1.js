/*
  Author: Lingyu Tang<tly629@outlook.com>
  Purpose: This program is for Conginss developer
            test part 1 task1
*/

const jsonfile = require('jsonfile');
const randomstring = require("randomstring");

const inputPath = './input2.json'
const outputPath = './output2.json'
jsonfile.readFile(inputPath, function (err, input) {
  var names = input.names;
  var emailArr = []
  for(var key in names){
    var email = getEmail(names[key]);
    emailArr.push(email);
  }
  jsonfile.writeFile(outputPath, {emails:emailArr})

})

function getEmail(name){
  var reverseName = reverseStr(name);
  var suffix = randomstring.generate(5) + "@gmail";
  return reverseName+suffix;
}

function reverseStr(str){
  var strArr = str.split("");
  var reverseArr = strArr.reverse();
  return reverseArr.join("");
}